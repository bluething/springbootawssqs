package com.wordpress.haelnote.springbootawssqs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootawssqsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootawssqsApplication.class, args);
	}

}

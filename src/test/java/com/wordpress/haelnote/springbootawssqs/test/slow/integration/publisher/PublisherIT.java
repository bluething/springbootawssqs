package com.wordpress.haelnote.springbootawssqs.test.slow.integration.publisher;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.Arrays;
import java.util.List;

import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.elasticmq.rest.sqs.SQSRestServer;
import org.elasticmq.rest.sqs.SQSRestServerBuilder;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.amazon.sqs.javamessaging.ProviderConfiguration;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;

@RunWith(SpringRunner.class)
@SpringBootTest
/*
 * Test annotation which indicates that the ApplicationContext associated with a test is dirty 
 * and should therefore be closed and removed from the context cache.
 * So it will forces the Application Context (and SQS connections) to reset between tests.
 * ElasticMQ will fail to stop if there are active connections.
 * */
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class PublisherIT {

	private static SQSRestServer sqsRestServer;
	private static String endPoint;
	private static AmazonSQS sqsClient;
	private static String queueUrl;

	@Autowired
	private WebApplicationContext webAppCtx;

	private MockMvc mockMvc;

	// pay attention to this!
	@BeforeClass
	public static void setup() {
		// creates an Elastic MQ Server on a port 40000
		sqsRestServer = SQSRestServerBuilder
				.withInterface("0.0.0.0")
				.withPort(40000)
				.start();
		// creates sqs endpoint
		endPoint = "http://localhost:" + sqsRestServer.waitUntilStarted().localAddress().getPort();
		// An SQS client for operating on queues outside of JMS listeners, etc. in app
		BasicAWSCredentials basicCredential = new BasicAWSCredentials("X", "X");
		sqsClient = AmazonSQSClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(basicCredential))
				.withEndpointConfiguration(new EndpointConfiguration(endPoint, ""))
				.build();
		// creates the queue before the Spring Application Context loads.
		queueUrl = sqsClient.createQueue("sampleQueue").getQueueUrl();

	}

	@AfterClass
	public static void cleanUp() {
		// delete queue
		sqsClient.deleteQueue(queueUrl);
		// wait the server to die
		sqsRestServer.stopAndWait();
	}

	@TestConfiguration
	static class TestQueueConfiguration {
		// pay attention to this!
		@Bean
		public SQSConnectionFactory sqsConnectionFactory() {
			ProviderConfiguration providerConfiguration = new ProviderConfiguration();
			BasicAWSCredentials basicCredential = new BasicAWSCredentials("X", "X");
			AmazonSQS sqs = AmazonSQSClientBuilder.standard()
					.withCredentials(new AWSStaticCredentialsProvider(basicCredential))
					.withEndpointConfiguration(new EndpointConfiguration(endPoint, ""))
					.build();
			return new SQSConnectionFactory(providerConfiguration, sqs);
		}
	}

	@Test
	public void createMessageGivenRightMessageSuccessPutToSQS() throws Exception {
		mockMvc = MockMvcBuilders.webAppContextSetup(webAppCtx).build();
		// given
		String value = "someValue";

		//when
		String uri = "/messages";

		@SuppressWarnings("unused")
		MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders
				.post(uri)
				.contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE)
				.content(EntityUtils.toString(new UrlEncodedFormEntity(Arrays.asList(
						new BasicNameValuePair("message", value)
						)))))
		.andReturn();

		//then
		AmazonSQS client = AmazonSQSClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials("X", "X")))
				.withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(endPoint, "elasticmq"))
				.build();
		ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(queueUrl)
				.withMaxNumberOfMessages(1)
				.withWaitTimeSeconds(3);
		List<Message> messages = client.receiveMessage(receiveMessageRequest).getMessages();

		assertThat(value, equalTo(messages.get(0).getBody()));
	}

}
